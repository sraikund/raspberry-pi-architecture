#a very small dhcp server

import socket
import utility
from constants.const import commands, ports


dhcpfile = "dhcp.txt"
hardcodedDHCPServerIP = "192.168.1.2" #this is a raspberry pi for now. That raspberry pi has this static ip.

class dhcp:
    takenIPs = []

    def __init__(self):
        IPList = readDHCPFile()
        self.takenIPs += IPList

    def newIP(self):
        for i in range(10, 256):
            if str(i) not in self.takenIPs:
                self.takenIPs += [str(i)]
                self.writeDHCPFile()
                return "192.168.1." + str(i) + "/24"

    def server(self):
        s = utility.getASock()
        utility.listen(s, ports['DHCP_PORT'], 100)
        utility.accept(s, receiveRequest, (self,))

    def writeDHCPFile(self):
        with open(dhcpfile, 'w') as f:
            for IP in self.takenIPs:
                print(IP)
                f.write("192.168.1." + str(IP) + "/24\n")



def receiveRequest(conn, dhcp):
    data, addr = conn.recvfrom(1)
    if bytes.decode(data) == "n":
        IP = dhcp.newIP()
        conn.sendall(str.encode(IP))


def requestNewIP():
    s = utility.getASock()
    s.connect((hardcodedDHCPServerIP, ports['DHCP_PORT']))
    s.sendall(str.encode("n"))
    while True:
        data, addr = s.recvfrom(1024)
        if data == b"":
            continue
        else:
            IP = data
            break
    return IP

def readDHCPFile():
    IPList = []
    with open(dhcpfile, 'r') as f:
        for line in f:
            if line[0] == "#": continue
            else:
                IP =  line.strip("\n").strip(" ")
                IP = IP.split("/")[0]
                ele = IP.split(".")
                IPList += [ele[3]]
    return IPList


def main():
    d = dhcp()
    d.server()


if __name__ == "__main__":
    main()
