# Simulation Installation Flow

## Orchestrator - Installation

1. mainOrchestrator.main() runs and enters the args.install conditional.

2. Simulation is loaded using argument from install option (Simulation.load())

    1. load() attempts to load a simulation object file from `<repo>/simulation/<simName>/simulation.obj`

        - "simulation.obj" is unpacked using pickle

    2. If no object file exists, a new one is created

        - loadPresetFile() reads the preset file from `<repo>/simulation/<simName>/preset.txt`

        - Preset information is used to create new Simulation object.

3. Simulation is installed by calling simInstance.installSimulation()

    1. For each Pi object in the simulation's piList, initialize the simulation directory (self.initSimulationDirectory())

        - Send the command `init <simName> COMMAND_END` to each of the Pi's.

    2. For each software listed in the simulation, create a list of IP addresses on which to install the software and send the package to each Pi (self.installSoftwareOnPis())

        - The software list contains the action to perform followed by the filename.  This is used to build a command and header.  This header is then sent to each IP in the IPlist.

## Node - Installation

1. mainNode.main() creates several threads to listen for incoming files and commands.  These threads each run socketUtil.acceptSocket(), which calls receive.receive() when a connection is made.

2.  receive.receive() determines how to treat the socket based on the port it is connected on

    1. If connected on FILE_PORT, use receive.receiveFile() to read the header and process the arguments before processing the file (receive.processFile())

        - The file path is calculated by appending the simulation/file name to the simulation path.  The program then spins until the folder is initialized, then writes the file.  Each of the CLI arguments are then processed.

            - If the path is specified, the file is moved to that location.  The path must be an absolute path, or relative to the caller (mainOrchestrator.py)

            - If a command is specified, the command is parsed by striping the '[]' from the front and back.  If the command starts with 'update', run the specified action.  Otherwise, create an executionCommand and execute it (catchAll)

            - If the delete flag is specified, delete the file we just wrote and/or moved.

            - If the type is specified, handle the file appropriately.

    2. If connected on COMMAND_PORT, receive data until the queue is empty, then create an execution command to run on the Pi.

        - Create an execution command (utility.ExecutionCommand()) which stores a command string and the execution type.

        - Execute the command (execute.executeCommand()) by determining the type and calling the appropriate function.

            - TERMINAL: pass the command to the system terminal, and return any error.

            - INIT: call initSimulation() which creates a SimulationProcess and adds the simulation name to the Pi's dockerSubnetList.

            - START:

            - STOP: kill the PIDs from the simulation and remove the simulation from the running list.

            - DELETE:

            - isAppSpecific: pipe the command to the process or Docker image running the simulation.

    3. If connected on KEY_SERVER_PORT, call keyClient.receiveCRL() to update the Pi's CRL.
