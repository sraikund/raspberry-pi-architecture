docker run --name mysim1 --network iocane-net $1:5000/$2
#--network allows overlaying user-defined network for dockers to talk
#name is important such that two containers do not have the same name in a given network
