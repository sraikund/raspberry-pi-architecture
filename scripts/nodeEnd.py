import nodeSetup
import paramiko
import sys

#This script kills all python processes to end each node

#Check nodeSetup to see where this fits in the workflow

def main():
	list1 = nodeSetup.getList()


	for ip in list1: #ssh into each pi on the list 
		ssh = paramiko.SSHClient()
		ssh.load_system_host_keys()
		ssh.connect(ip, username = "pi", password = "raspberry")
		ssh.exec_command("pkill -9 python")


if __name__== '__main__':
	main()
