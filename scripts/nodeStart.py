import nodeSetup
import paramiko
import sys

#This script starts the listener function on each node and prints the output
#to a file

#See nodeSetup for where this fits in the workflow

def main():
	if len(sys.argv)!=2:
		print("Error provide Orchestrator IP address")
		sys.exit(0)
	OrchestratorIP = sys.argv[1]
	list1 = nodeSetup.getList()


	for ip in list1: #ssh into each pi on the list 
		ssh = paramiko.SSHClient()
		ssh.load_system_host_keys()
		ssh.connect(ip, username = "pi", password = "raspberry")
		#ssh.exec_command("cd raspberry-pi-architecture/src/; python3 mainNode.py " + OrchestratorIP + " 2>&1 | tee simulation/output.txt" )
		#ssh.exec_command("cd raspberry-pi-architecture/src/; touch hello")
		stdin,stdout,stderr = ssh.exec_command("cd raspberry-pi-architecture/src/; screen -d -L -m -t  scre python3 mainNode.py " + OrchestratorIP + " > output2.txt ")
if __name__== '__main__':
	main()
