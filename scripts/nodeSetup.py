import paramiko

#This script gets downloads the gitlab code and runs certgen to create 
#an SSL connection

# a typical workflow should look like the following: nodeSetup -> nodeStart
#-> mainOrchestrator.py --install -> mainOrchestrator.py --start -> nodeEnd

def getList():
	f = open("iplist.txt","r")
	contents = f.read()
	list1 = contents.replace("\n", " ")
	list1 = list1.split() #import list of ipaddresses
	return list1

def blockingssh_exec(client, string):
	print(string)
	stdin, stdout, stderr = client.exec_command(string)
	exit_status = stdout.channel.recv_exit_status()

def main():
	list1= getList()
	for ip in list1: #ssh into each pi on the list 
		ssh = paramiko.SSHClient()
		ssh.load_system_host_keys()
		ssh.connect(ip, username = "pi", password = "raspberry")
	
		blockingssh_exec(ssh, "rm -rf raspberry-pi-architecture")
		blockingssh_exec(ssh, "git clone https://gitlab.com/sraikund/raspberry-pi-architecture.git")
		blockingssh_exec(ssh, "cd raspberry-pi-architecture/scripts; sh certgen.sh " +ip)


if __name__== '__main__':
	main()
