## Creating Certificates

**Note:** The CA cert and key are provided so nodes can regenerate and self-sign their own certificates as needed.  In essence, every node is a member of the CA.  In actual use, the CA would be a separate entity and the CA key would be unknown to the nodes.

Since every device on the network needs to agree on a CA, the CA cert and key should remain constant.  If a new CA cert is required, it must be added to all nodes, and all node certificates will have to be regenerated using the new CA cert.

#### Setting up Certificate Authority
*Only do this if the CA is changing, this will invalidate all previous certificates*
1. Move into the certs folder in the repo  
`cd <repo>/certs`

2. Create a private key for the CA  
`openssl genrsa -out cakey.pem 2048`

3. Create the CA certificate.  You will be prompted for location and hostname information, the importance of this is unclear.  
`openssl req -new -x509 -key keys/cakey.pem -out certs/cacert.pem`

#### Request a new Signed Certificate
1. Move into the certs folder in the repo  
`cd <repo>/certs`

2. Create a Certificate Signing Request for this node.  This will generate a new private key and place it in `./keys/pikey.pem`    
`openssl req -new -sha256 -out pi.csr -keyout keys/pikey.pem`

3. Ask the CA for a signed certificate  
*If the CA is an independent entity, send it `pi.csr` from step 2, and have it run the command below.*  
*If the CA is "distributed" (i.e. CA key is present on all devices) run the command from the device.*  
`openssl x509 -req -in pi.csr -CA certs/cacert.pem -CAkey keys/cakey.pem -CAcreateserial -out certs/picert.pem`
