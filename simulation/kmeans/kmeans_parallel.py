import csv
import time, math
from mpi4py import MPI
import numpy as np
from numpy import genfromtxt

data = genfromtxt('data_set.csv', delimiter=',', dtype=float)
# print(data)
# print(len(data))
true_cluster = genfromtxt('true_clusters.txt', delimiter=',', dtype=int)
# print(true_cluster)

num_clusters = 9  # Number of centers in dataset


def initialize_centroids(points, k):
    """returns k centroids from the initial points by copying the set of points and shuffling"""
    centroids = points.copy()
    np.random.shuffle(centroids)
    return centroids[:k]


def create_clusters(points, centroids):
    """Returns an array containing the index to the nearest centroid for each point"""
    # clusters = np.zeros(len(num_clusters))
    # for i in range(len(num_clusters)):
        # value = euclidean_distance(centroids[i], points)
    distances = np.sqrt(((points - centroids[:, np.newaxis])**2).sum(axis=2))
    return np.argmin(distances, axis=0)


def move_centroids(points, closest, centroids):
    """Returns an array of updated centroids assigned from the points closest to them"""

    return np.array([points[closest == k].mean(axis=0) for k in range(centroids.shape[0])])


def euclidean_distance(a, b):
    """Returns the euclidean distance between 2 points using numpy"""
    sum = 0.0
    for i in range(len(a)):
        sum += np.linalg.norm(a - b)
    final = math.sqrt(sum)
    return final


def stop(centroids, new_centroids, cutoff, k):
    """Returns a boolean true if the new centroids are far enough away to continue iterations"""
    for i in range(k):
        difference = euclidean_distance(centroids[i], new_centroids[i])
        return difference > cutoff


def main():
    """The master partitions the data into P subgroups of equal size. For each P a new process is created
    and the list of centroids is sent to the created process. Each node calculates the distances
    and assigns new clusters. Each process receives cluster members of k clusters from P processes to
    recalculate the new centroids which is sent back to the master node"""

# Initialize MPI variables
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()

    start = MPI.Wtime()  # start time for calculating execution time
    print("Process %d started. \n" % rank)
    centroids = np.zeros(9)

    if rank == 0:
        print("Start")
        # Loop through all nodes to distribute centroids
        for i in range(1, size):
            centroids = initialize_centroids(data, num_clusters)
            comm.send(centroids, dest=rank, tag=rank)
            print("Data sent to worker %d .\n" % i)
            print(centroids)
        print("Data sent to all workers. \n")

    comm.Barrier()  # Wait until all data has been sent

    if rank != 0:
        cutoff = .01
        print("Data received from process %d .\n" % rank)
        received = comm.recv(source=0, tag=rank)
        print(received)
        for i in range(1, size):
            new_centroids = comm.recv(source=0, tag=rank)
            print(new_centroids)
            if stop(centroids, new_centroids, cutoff, num_clusters):
                # Update the clusters based on the new centroids
                centroids = new_centroids
                clusters = create_clusters(data, centroids)
                # Recalculate the centroids using the new clusters
                new_centroids = move_centroids(data, clusters, centroids)
                print(new_centroids)
            else:
                print("Final Centroids:")
                # send updated centroids to master
                comm.send(new_centroids, dest=0, tag=rank)
                print(new_centroids)
                print(euclidean_distance(centroids[1], new_centroids[1]))

    comm.Barrier()  # Wait until all data has been received by nodes and sent back to master

    if rank == 0:
        distances = np.zeros(9)
        print("Checking responses from nodes .\n")
        for i in range(1, size):
            # Receive centroids from nodes
            new_centroids = comm.recv(source=1, tag=rank)
            print(new_centroids)
            # find the total euclidean distance
            for j in range(num_clusters):
                difference = euclidean_distance(centroids[j], new_centroids[j])
                print(difference)

    end = MPI.Wtime()
    print('time:', end - start)

    comm.Barrier()  # Wait until all data has been received by master


if __name__ == "__main__":
    main()

