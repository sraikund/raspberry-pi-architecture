import csv
import time, math
from mpi4py import MPI
import numpy as np
from numpy import genfromtxt

data = genfromtxt('data_set.csv', delimiter=',', dtype=float)
# print(data)
# print(len(data))
true_cluster = genfromtxt('true_clusters.txt', delimiter=',', dtype=int)
# print(true_cluster)

num_clusters = 9  # Number of centers in dataset


def initialize_centroids(points, k):
    """returns k centroids from the initial points by copying the set of points and shuffling"""
    centroids = points.copy()
    np.random.shuffle(centroids)
    return centroids[:k]


def create_clusters(points, centroids):
    """Returns an array containing the index to the nearest centroid for each point"""
    # clusters = np.zeros(len(num_clusters))
    # for i in range(len(num_clusters)):
        # value = euclidean_distance(centroids[i], points)
    distances = np.sqrt(((points - centroids[:, np.newaxis])**2).sum(axis=2))
    return np.argmin(distances, axis=0)


def move_centroids(points, closest, centroids):
    """Returns an array of updated centroids assigned from the points closest to them"""

    return np.array([points[closest == k].mean(axis=0) for k in range(centroids.shape[0])])


def euclidean_distance(a, b):
    """Returns the euclidean distance between 2 points using numpy"""
    sum = 0.0
    for i in range(len(a)):
        sum += np.linalg.norm(a - b)
    final = math.sqrt(sum)
    return final


def stop(centroids, new_centroids, cutoff, k):
    """Returns a boolean true if the new centroids are far enough away to continue iterations"""
    for i in range(k):
        difference = euclidean_distance(centroids[i], new_centroids[i])
        return difference > cutoff


def main():
    """The master partitions the data into P subgroups of equal size. For each P a new process is created
    and the list of centroids is sent to the created process. Each node calculates the distances
    and assigns new clusters. Each process receives cluster members of k clusters from P processes to
    recalculate the new centroids which is sent back to the master node"""
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()

    start = MPI.Wtime()  # start time for calculating execution time
    print("Process %d started. \n" % rank)

    if rank == 0:
        print("Start")
        # Loop through all nodes to distribute centroids
        centers = np.zeros(size)
        for i in range(1, num_clusters):
            centers = initialize_centroids(data, num_clusters)
            comm.send(centers, dest=i, tag=i)
            print("rank", rank, centers)
        print("Data sent to all workers. \n")
    #else:
        #centers = None

    if rank != 0:
        print("Data received from process %d .\n" % rank)
        # centers = comm.scatter(centers, root=0)
        print("rank", rank, "has data", centers)
        new_centroids = comm.recv(source=0, tag=rank)
        print("rank", rank, "has data", new_centroids)
        

    comm.Barrier()

    end = MPI.Wtime()
    print('time:', end - start)


if __name__ == "__main__":
    main()








