import argparse
import sys
import threading
from orchestrator.simulation import *
import socketUtil
import utility
import json
from sendUpgrade import send
from files import files
import receive
from multiprocessing import pool, Lock
from constants import OPENC2


lock = Lock()


def main():
    parser = argparse.ArgumentParser()
    # send_type = ''
    parser.add_argument('serialization', nargs='?')
    t = sys.argv[1:]
    args = parser.parse_args(t)
    if args.serialization is None:
        send_type = OPENC2.JSON
    else:
        if utility.check_serial(args.serialization):
            send_type = args.serialization
        else:
            raise ValueError("Not a valid serialization type")
    IPList = []
    port = ports.OPENC2_PORT
    filename = utility.getRepoPath() + '/' + files.PEERLIST_FILE
    with open(filename, 'r') as f:
        for line in f:
            IPList = line.strip("\n").split(",")
    print('----------------')
    print('Sending OpenC2 commands to IP(s):')
    for ip in IPList:
        print('   ', ip + ':' + str(port))
    while True:
        valid_command = False
        # input_str = ''
        input_str_lst = []
        msg = {}
        # serial_type = ''
        while not valid_command:
            print("Enter an OpenC2 command to send system-wide")
            input_str = input()
            input_str_lst = input_str.split(" ")
            valid_command = utility.check_valid(input_str_lst[0])
            if not valid_command:
                print("Not a valid command, try again")
        if input_str_lst[0] == 'quit' or input_str_lst[0] == 'q':
            print("Exiting OpenC2 CLI")
            break
        elif input_str_lst[0] == "help" or input_str_lst[0] == "h" \
                or input_str_lst[0] == "help " or input_str_lst[0] == "h ":
            pass  # TODO Fix later
        else:
            user_cmd = {"Action": input_str_lst[0], "Target": input_str_lst[1],
                        "Parameters": input_str_lst[2:]}
            msg = utility.SerialMessage(user_cmd["Action"], user_cmd["Target"], user_cmd["Parameters"])
            msg.set_type(send_type)
            msg.set_msg()
        for ip in IPList:
            response = ''
            send_sock = socketUtil.createClientSideSocket(ip)
            send_pool = pool.ThreadPool(processes=1)
            async_result = send_pool.apply_async(send, (send_sock, ip, port, commands.OPENC2, msg))
            try:
                response = bytes.decode(async_result.get())
            except TypeError:
                print("Response not received")
                pass
            print(response)
        print('----------------')
        time.sleep(2)


if __name__ == "__main__":
    main()
