class commands:
    #command types for cli
    ALLPEERS = "a"
    EXECUTION_COMMAND = '-c'
    FILE_COMMAND = '-f'
    STRING_FILE_COMMAND = '-s'

    #file types for header
    DOCKER_IMAGE = 'image'
    SCRIPT = 'script'
    PYTHON = 'python'
    TAR = 'tar'
    OTHER = 'file'

    #upgrade
    #keywards with upgrade
    UPDATE = "update"
    RUN = "run"
    SAVE = "save"

    #execution types.
    INIT = "init" #get ready for installing a new simulation
    TERMINAL = "terminal"
    DOCKER = "docker"
    REMOVE = "remove"
    START = "start"
    STOP = "stop"
    DELETE = "delete"
    SCAN = "scan"
    REPORT = "report"
    DENY = "deny"
    SNAPSHOT = "snapshot"
    DETONATE = "detonate"
    RESTORE = "restore"
    ADDPEERGOSSIPUPGRADE = "addpeer"
    SHUTDOWN = "shutdown"

    #query commmand and subcommands. query [-g or -s] [item]
    QUERY = "query"
    GENERAL = "-g"
    SPECIFIC = "-s"

    #items
    PEERS = "peers" #peers
    RANKING = "ranking" #ranking of specific peer
    HARDWARE = "hardware" #hardware data (cpu tempature for example)

    #key server
    KEY_SERVER_REQUEST = "KEY_SERVER_REQUEST"
    SIGN_REQUEST = "a"
    RENEWAL_REQUEST = "b"
    REVOKE_REQUEST = "c"
    CRL_DATA = "d"

    #preset file
    BRIDGELOCATION = "bridgeLocation"

    #end of command
    COMMAND_END = "COMMAND_END"
    DATA_END = b"DATA_END" #in bytes

    #errors
    COMMAND_NOT_AVAILABLE = "COMMAND_NOT_AVAILABLE"

    # OpenC2
    OPENC2 = "OpenC2"
    SERIAL = "serial_type"


# OpenC2 Serialization Type
class OPENC2:
    JSON = 'json'
    MSG_PACK = 'msgpack'
    SERIAL_LST = [JSON, MSG_PACK]

class ports:
    BUFFER_SIZE = 4096
    DHCP_PORT = 999

    COMMAND_PORT = 4000
    FILE_PORT = 4001
    DATA_PORT = 4100
    RESPONSE_PORT = 4200

    KEY_SERVER_PORT = 6000
    KEY_SERVER_RESPONSE = 6001
    KEY_SERVER_UNENCRYPTED = 6002

    OPENC2_PORT = 7000
    LOCAL_OPENC2_PORT = 9000


class IP:
    KEY_SERVER = "192.168.1.11"
    #ORCHESTRATOR = "192.168.0.101" #TODO change to correct IP eventually
