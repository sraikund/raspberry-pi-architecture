#a bridge between this recieiving program and the simulation program.
# Functions include getPeerList,

import argparse
import socket
import ssl

from constants import commands, ports
import socketUtil
import utility

def receiveRequest():
    '''Open a socket on the OpenC2 port and wait for an incoming request.'''

    sock, context = socketUtil.createServerSideSocket(ports.OPENC2_PORT, 1)
    # manually accept and wrap the socket because we want to manipulate the data directly
    newSocket, fromAddr = sock.accept()
    conn = context.wrap_socket(newSocket, server_side=True)

    # read data as long as there is data to read
    request = receiveData(conn)
    return request


def receiveData(sock):
    assert isinstance(sock, ssl.SSLSocket)

    data = ''
    packet = sock.recv(ports.BUFFER_SIZE)
    while packet:
        data += bytes.decode(packet)
        packet = sock.recv(commands.BUFFER_SIZE)

    return data


def pipeToProcess(myPi, cmd):
    '''Pass a command to the process or Docker container running a simulation.

    Most commands will have to be piped to the simulation. Those that don't are commands
    that deal with the actual running of a process.

    @param myPi Pi object representing node to send command to
    @param cmd  raw command string to pipe to myPi
    '''
    # set up command argument parsing
    parser = argparse.ArgumentParser()
    parser.add_argument("--name", nargs=1, type=str)
    args = parser.parse_args(cmd.split(' '))

    # get simulation or raise an error if command is invalid
    if args.name:
        simName = args.name
    else:
        raise IOError

    sim = myPi.getSimulation(simName)
    type, location = utility.getProgramLocation(sim.simName)

    if type == "docker":  # location is the container name
        cont = location
        subDict = myPi.dockerSubnetList[simName]
        IP = subDict[cont]

        # create a socket to send the command and read the response
        sock = socketUtil.createClientSideSocket(IP)
        sock.connect((IP, ports.LOCAL_OPENC2_PORT))

        sock.sendall(cmd)
        response = receiveData(sock)
        return response

        # TODO: this used to open a new ServerSide socket for the reponse,
        # instead of listening on the same connection that the data was sent on.
        # If this was the desired behavior, use the code below...
        # sock, context = socketUtil.createServerSideSocket(ports.LOCAL_OPENC2_PORT, 1)
        # conn, addr = sock.accept()
        # responseData = receiveData(conn)
        # return responseData   # has to be send back to orchestrator
    elif type == "file":
        # TODO: containerless simulation
        pass
    # determine if docker or not docker


# """
# gets the peer list from the simulation.
# For example, nodes in a BGP simulation have peers which share routing updates
# """
# def getPeerList(myPi, simulation):
#     type, location = utility.getProgramLocation(simulation.simName)
#
#     module = None
#     if type is None:
#         return None
#     elif type == "docker":
#         #we have to do container communication to the container called location
#         containerIPDict = myPi.getSubnet(simulation.simName)
#         dataContainerName = containerIPDict[simulation.dataContainerName]
#         IP = containerIPDict[dataContainerName]
#         s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#         s.connect((IP, OPENC2_PORT))
#         s.sendall(str.encode(GETPEERLIST))
#         receieveDockerResponse(PEERLISTFILE)
#     elif type == "file":
#         #location is a file
#         module = __import__(location)
#         module.getPeerList()
#
