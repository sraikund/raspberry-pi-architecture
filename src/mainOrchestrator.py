"""
Starts the orchestrator, loads simulation and installs it on pis if not installed, and listens for incoming pidata
There are 4
"""
import argparse
import socket
import ssl
import sys
import threading
from threading import *

from constants import commands, ports
from orchestrator.simulation import *
import socketUtil
import utility
from pi import *


def main():
    '''
    There are 4 modes of running mainOrchestrator:
    1. python3 mainOrchestrator.py --install simName              this installs the simulation stored in simulation/simName on all the pis. Exits afterwards
    2. python3 mainOrchestrator.py --start simName                this starts the simulation called simName on all the pis and then starts the analyzer and the command line in different threads
    3. python3 mainOrchestrator.py --cmd                          this starts the command line only
    4. python3 mainOrchestrator.py --analyze                      this continues analyzing whatever simulations are currently live
    '''

    parse = argparse.ArgumentParser()
    parse.add_argument('--install')
    parse.add_argument('--start')
    parse.add_argument('--cmd', action='store_true')
    parse.add_argument('--remove')
    parse.add_argument('--analyze')
    t = sys.argv[1:]
    args = parse.parse_args(t)
    
    # we can't start and install a simulation at the same time
    if args.start is not None and args.install is not None:
        raise IOError
    # install a simulation
    elif args.remove is not None:
        sim = Simulation.load(args.remove)
        sim.removeSimulation(args.remove)
    elif args.install is not None:
        os.system("docker ps -a | grep registry && docker container stop registry && docker container rm -v registry")
        sim = Simulation.load(args.install)
        sim.removeSimulation(args.install)
        sim = Simulation.load(args.install)
        #sim.type = "cmd"
        sim.installSimulation()
    # enter command line
    # elif args.cmd:
    #     print("The openC2 commandline. Files and commands can be manually send to pis here. q to quit")
    #     sendUpgrade.main()

    # starting and analyzing share most of their process, so they are combined here
    if args.start or args.analyze:
        # start specific code...
        if args.start:
            sim = Simulation.load(args.start)
            sim.runSimulation()
            print('Running simulation: ' + sim.simName)
        # analyze specific code...
        else:
            sim = Simulation.load(args.analyze)

        # print("The openC2 commandline. Files and commands can be manually send to pis here. q to quit")

        # create a socket and SSLContext
        # sock, context = socketUtil.createServerSideSocket(ports.RESPONSE_PORT)

        # create two threads to accept connections and process data on
        # the newly created socket, and to process the CLI inputs
        # analysisThread = Thread(target=socketUtil.acceptSocket, args=(sock, context, analysis.beginIncomingDataThread, (sim)))
        commandLineThread = Thread(target=sendUpgrade.main())

        # start the two new threads
        #analysisThread.start()
        commandLineThread.start()

    # Sanity check to verify the threads launched successfully
    print('----------------')
    print('Running threads:')
    for thread in threading.enumerate():
        print(thread)
    print('----------------')


if __name__ == '__main__':
    main()
