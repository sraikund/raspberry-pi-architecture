#!/usr/bin/env python3
import os
import OpenSSL.crypto
import pickle
from threading import Thread
import time
import sys

from constants import commands, ports
from files import paths, files
import keyClient
from pi import *
import receive
import socketUtil
import utility


def main():
    '''
    Sets up directories to create a pi object and create a new pikey and picert if they do not exist already.
    Create sockets to listen on command, file, and keyserver ports. Start three threads for each port when connections
    are made and wait for clients to bind.
    :return:
    '''
    if len(sys.argv) <2:
        print("Invalid number of inputs Usage: <OrchestratorIPAddress>")
        sys.exit(0)
        
    OrchestratorIP=sys.argv[1]
    print (OrchestratorIP)
    # set up directories and create a Pi object
    myPi = init(OrchestratorIP) #TODO need to receive the CRL
    keyInit(myPi) #initialize keys

    # create three sockets to listen for data on the Command, File, and KeyServer ports
    # Each call to createServerSideSocket() returns a new context, but they are the same,
    # hence the reuse of the same `context` variable for each.
    commandSocket,   context = socketUtil.createServerSideSocket(ports.COMMAND_PORT)
    fileSocket,      context = socketUtil.createServerSideSocket(ports.FILE_PORT)
    keyServerSocket, context = socketUtil.createServerSideSocket(ports.KEY_SERVER_PORT)
    openC2_socket, context = socketUtil.createServerSideSocket(ports.OPENC2_PORT)

    # Create separate threads for each socket to accept new connections and process data
    # Target is the function that will be called by the new thread, and args is the list of arguments for that function.
    # socketUtil.acceptSocket() itself takes a function and arguments, which are the last two elements of the args parameter
    commandThread   = Thread(name='CommandThread',   target=socketUtil.acceptSocket, args=(commandSocket,   context, receive.receive, (myPi,)))
    commandThread.daemon = True
    fileThread      = Thread(name='FileThread',      target=socketUtil.acceptSocket, args=(fileSocket,      context, receive.receive, (myPi,)))
    fileThread.daemon = True
    # keyServerThread = Thread(name='KeyServerThread', target=socketUtil.acceptSocket, args=(keyServerSocket, context, receive.receive, (myPi,)))
    openc2Thread = Thread(name='openc2Thread', target=socketUtil.acceptSocket, args=(openC2_socket, context,
                                                                                     receive.receive, (myPi,)))
    openc2Thread.daemon = True #setting threads daemon to true makes the thread close upon exit of the main thread. It does not cleanup on its own however (TODO?)

    # Start each of the threads we just created
    fileThread.start()
    commandThread.start()
    openc2Thread.start()
    #keyServerThread.start()

    # Sanity check to verify the threads launched successfully
    print('----------------')
    print('Running threads:')
    for thread in threading.enumerate():
        print(thread)
    print('----------------')
    
    while True: #keep the main thread going so that the child threads do not exit 
        keyboardinput=input()
        if keyboardinput == "quit" or keyboardinput == "q": #once quit is found exit main threads so all other threads close
             print("Stopping all threads")
             break
                 
			

def init(OrchestratorIP):
    '''
    Create paths in the repository for the project and docker image if they do not exist already.
    :return: newPi with the same ip address of the current pi if there is an error with the config file and are unable
    to successfully read the data file
    '''
    projectPath = utility.getRepoPath()
    docker_path = projectPath + paths.DOCKER_IMAGES
    temp_path = projectPath + paths.TEMP_DIR

    # Create the above directories in the root repository folder.
    # If they already exist, just move on and don't raise an error
    os.makedirs(docker_path, exist_ok=True)
    os.makedirs(temp_path, exist_ok=True)

    # assume an existing Pi, unless the data file is missing,
    # empty or starts with the line 'NEW'
    createNewPi = False
    if os.path.exists(files.piDataFile):
        try:
            with open(files.piDataFile, 'rb') as f:
                line = f.readline()
                if line == 'NEW' or os.stat(files.piDataFile).st_size == 0:
                    createNewPi = True
        except:
            pass
    else:
        createNewPi = True

    # attempt to read the Pi from the data file
    # if successful, return the object, otherwise
    # create a new Pi object from scratch
    if not createNewPi:
        with open(files.piDataFile, 'rb') as f:
            myPiState = pickle.load(f, encoding='UTF-8')
        if isinstance(myPiState, PiState):
            myPi = Pi(utility.getIP(),OrchestratorIP)
            myPi.setCurrentState(myPiState)
            return myPi

    # create a new Pi (either no config file
    # or reading the config failed)
    currentIP = utility.getIP()
    newPi = Pi(currentIP,OrchestratorIP)
    state = PiState([], currentIP, None, time.asctime())
    newPi.setCurrentState(state)

    # with open('../certs/crl/crl.pem', 'rb') as f: crlBytes = f.read()
    # newPi.setCRL(utility.loadCRLFromBytes(crlBytes)) #TODO !!! TEMPORARY !!! CHANGE

    return newPi


def keyInit(myPi):
    createNewKey = False
    createNewCert = False

    #check if keys are made. If not make key and cert
    if not os.path.exists(utility.getPiKeyPath()):
        createNewCert = True
        createNewKey = True
    #check if cert is made. If not make cert
    elif not os.path.exists(utility.getPiCertPath()):
        createNewCert = True
    #check if cert is expired or revoked
    else:
        with open(utility.getPiCertPath(), "rb") as f: piCertBytes = f.read()
        if not keyClient.isValidCert(piCertBytes, myPi):
            #create new cert.
            createNewCert = True
        else:
            myPi.myCert = utility.loadCert_Cryptography(piCertBytes)


    if createNewKey:
        privKeyBytes = utility.genRSAKeyFile(utility.getPiKeyPath())
        privKey = utility.loadPrivKey_OpenSSL(OpenSSL.crypto.FILETYPE_PEM, privKeyBytes, b'password')
    else:
        with open(utility.getPiKeyPath(), 'rb') as f: privKey = f.read()
        privKey = utility.loadPrivKey_OpenSSL(OpenSSL.crypto.FILETYPE_PEM, privKey, b'password')

    if createNewCert:
        #pubkey = privKey.
        #pubkey = utility.loadPubKey_OpenSSL(OpenSSL.crypto.FILETYPE_PEM, )
        #try:
        cert = keyClient.createCert(myPi, privKey)
        myPi.myCert = cert
        #except Exception("KEY_SERVER_DOWN") as e:
         #   return e



#TODO: find a way to call this
def exit():
    os.system("sudo rm -r tempDir")


if __name__ == '__main__':
    main()
