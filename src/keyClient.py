"""
Communicates with the key server:
1. Sends requests to sign, renew, or revoke certificates.
2. Receives updated CRLs
3. Checks if a certificate is valid (in general and against a CRL)
"""

from __future__ import print_function, unicode_literals
from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives import hashes
import cryptography.x509.extensions as extensions
import datetime
import OpenSSL.crypto

import socketUtil
from constants import commands, ports, IP
from files import *
import sendUpgrade
import utility

"""
generate a new CSR using the CSR DN information that is stored in the CSR preset file
"""
def generateCSR(privkey):
    '''
    Generate a new CSR using the CSR DN information that is stored in the CSR preset file
    :param privkey: private key of the pi
    :return: certificate signing request
    '''
    # if there is a password, that is the 3rd arg
    req = OpenSSL.crypto.X509Req()

    #before adding data, get it from preset file
    csrDict = utility.loadCSRFile(files.CSR_PRESET_FILE)
    if "C" in csrDict: #country
        req.get_subject().C = csrDict["C"]
    elif "ST" in csrDict: #state
        req.get_subject().ST = csrDict["ST"]
    elif "L" in csrDict: #locality
        req.get_subject().L = csrDict["L"]
    elif "O" in csrDict: #organization
        req.get_subject().O = csrDict["O"]
    elif "OU" in csrDict: #organization unit/dept
        req.get_subject().OU = csrDict["OU"]
    #set CN to ip address
    IP = utility.getIP()
    req.get_subject().CN = IP
    req.set_pubkey(privkey)
    req.sign(privkey, "sha256")
    ftype = OpenSSL.crypto.FILETYPE_PEM
    csr = OpenSSL.crypto.dump_certificate_request(ftype, req)
    with open(utility.getCSRPath(), 'wb') as f:
        f.write(csr)
    return csr


def createCert(myPi, privKey):
    '''
    Creates a new cert by creating a CSR and asking the key server to sign it

    :param myPi:
    :param privKey: a privkey object
    :return:
    '''
    csr = generateCSR(privKey)
    data = buildRequestData(commands.SIGN_REQUEST, (csr,))
    newCertData = keyServerRequest(myPi, data, commands.SIGN_REQUEST, ports.KEY_SERVER_UNENCRYPTED)
    newCert = utility.loadCert_Cryptography(str.encode(newCertData))
    if newCert == None:
        return Exception("KEY_SERVER_DOWN")
    myPi.myCert = newCert
    with open(utility.getPiCertPath(), 'w') as f:
        f.write(newCertData)
        f.flush()
    return newCert

def generateCACert():
    with open(utility.getCaKeyPath(), 'rb') as f: privKey = f.read()
    privKeyCryptography = utility.loadPrivKeyCryptography(privKey)
    privKeyOpenSSL = utility.loadPrivKeyOpenSSL(OpenSSL.crypto.FILETYPE_PEM, privKey, passphrase=None)
    csr = generateCSR(privKeyOpenSSL)
    csr = utility.loadCSR_Cryptography(csr)
    subject = csr.subject

    keyUsageExt = x509.KeyUsage(digital_signature=True, content_commitment=True, key_encipherment=False, data_encipherment=False, key_agreement=False, key_cert_sign=True, crl_sign=True, encipher_only=False, decipher_only=False)

    cert = x509.CertificateBuilder() \
        .subject_name(subject) \
        .issuer_name(subject) \
        .public_key(csr.public_key()) \
        .serial_number(x509.random_serial_number()) \
        .not_valid_before(datetime.datetime.utcnow()) \
        .not_valid_after(datetime.datetime.utcnow() + datetime.timedelta(days=365)) \
        .add_extension(keyUsageExt, critical=True) \
        .sign(privKeyCryptography, hashes.SHA256(), default_backend())

    with open("cacert.pem", 'wb') as f: f.write(cert.public_bytes(serialization.Encoding.PEM))

    return cert

def keyServerRequest(myPi, data, sendType, port):
    #get keyServerIP
    keyServerIP = IP.KEY_SERVER
    #send data
    if port == ports.KEY_SERVER_UNENCRYPTED:
        sock = socketUtil.createClientSideSocket(IP, secure=False)
    else:
        sock = socketUtil.createClientSideSocket(IP)

    response = sendUpgrade.send(sock, keyServerIP, port, sendType, data)
    print("bottom of keyServerRequest:", response)
    #a response is done inside sendUpgrade.sendKeyServerRequest
    return response

def generateRevocation(certFileName, reasonFlag):
    '''
    Revoke a certificate if a reason flag is raised
    :param certFileName: file containing cert data
    :param reasonFlag: a flag raised if cert is expired or should be revoked
    :return: revoked certificate object
    '''
    #load certificate to get serial
    with open(certFileName, "rb") as f:
        pem_data = f.read()
    cert = x509.load_pem_x509_certificate(pem_data, default_backend())
    serial = cert.serial_number

    builder = x509.RevokedCertificateBuilder()
    #set time/date as current time/date
    builder = builder.revocation_date(datetime.datetime.today())
    #set CRL reason extension
    reason = x509.CRLReason(reasonFlag)
    builder = builder.add_extension(reason, critical=True)
    builder = builder.serial_number(serial)
    revoked_certificate = builder.build(default_backend())
    return revoked_certificate


"""
checks whether or not a certificate is revoked (in a CRL).
This is done by comparing serial numbers of the cert with serial numbers of the revoked certs in the CRL
"""
def isCertInCRL(cert, crl):
    #todo remember to lock/acquire crl before calling this function
    for revoked in crl:
        if cert.serial_number == revoked.serial_number:
            return True
    return False


def isValidCRL(crl, caCert):
    '''
    Checks if a CRL is valid by checking if the signature is correct and corresponds the public key of the CA
    :param crl: CRL object
    :param caCert: certificate authority's certificate object through which public key is found
    :return: boolean True if the CRL is valid
    '''
    # check signature
    publicKey = caCert.public_key()
    if not crl.is_signature_valid(publicKey):
        return False

    return True

def receiveCRL(conn, myPi):
    """
    A pi runs receiveCRL to receive a new CRL from a keyserver.
    The keyServer sends a new CRL every certain amount of time if their has been a revoked certificate in that period of time
    """

    crlbytes = b''

    while True:
        data = conn.recv(ports.BUFFER_SIZE)
        if data == commands.DATA_END: break
        crlbytes += data

    request = bytes.decode(crlbytes)[0]
    crlbytes = str.encode(bytes.decode(crlbytes)[1:])

    if request == commands.CRL_DATA:
        crl = utility.loadCRLFromBytes(crlbytes)
        with open(utility.getCaCertPath(), "r") as f:
            caCertData = f.read()
        caCertDataBytes = str.encode(caCertData)
        caCert = utility.loadCert_Cryptography(caCertDataBytes)

        if isValidCRL(crl, caCert):
            myPi.setCRL(crl)
            with open(utility.getCRLPath(), "w") as f:
                f.write(bytes.decode(crlbytes))
        else:
            raise Exception("invalid CRL")


def buildRequestData(requestType, requestParam):
    """
    builds the request that will be sent to the key server.
    request type is either SIGN_REQUEST, RENEWAL_REQUEST, or REVOKE_REQUEST
    request params is a tuple of parameters that is dependent on the request type
    """
    request = b''
    if requestType == commands.SIGN_REQUEST:
        csr = requestParam[0]
        request = str.encode(commands.SIGN_REQUEST) + csr
    elif requestType == commands.RENEWAL_REQUEST:
        pass     # TODO: we can just use sign and revoke for now
    elif requestType == commands.REVOKE_REQUEST:
        revocation = generateRevocation(requestParam[0], requestParam[1])
        date = revocation.revocation_date
        serial = revocation.serial_number
        reason = requestParam[1].value
        request = str.encode(commands.REVOKE_REQUEST) + str.encode(str(serial)) + str.encode(";") + str.encode(str(date)) + str.encode(";") + str.encode(reason)
    else:
        raise IOError

    return request


def request(myPi, requestType):
    '''
    completes a request for either a renewal, csr sign, or revocation.
    This includes creating the request and sending it
    :param myPi:
    :param requestType:
    :return:
    '''
    if requestType == commands.SIGN_REQUEST:
        with open(utility.getPiKeyPath(), "r") as f:
            keyData = f.read()
        ftype = OpenSSL.crypto.FILETYPE_PEM
        key = OpenSSL.crypto.load_privatekey(ftype, keyData)
        csr = generateCSR(key)
        buildRequestData(commands.SIGN_REQUEST, csr)
        keyServerIPList = myPi.keyServerIPList   #TODO needs to be completed
        sendUpgrade.sendAll()
    elif requestType == commands.RENEWAL_REQUEST:
        pass
    elif requestType == commands.REVOKE_REQUEST:
        pass

def isValidCert(certDataBytes, myPi):
    """
    Checks if certificate is valid
    :param certDataBytes: certificate data in bytes
    :param myPi: pi object
    :return: bool -- whether or not cert if valid
    """
    pyCryptographyCert = utility.loadCert_Cryptography(certDataBytes)
    certDataStr = bytes.decode(certDataBytes)
    pyopensslCert = utility.loadCert_OpenSSL(OpenSSL.crypto.FILETYPE_PEM, certDataStr)

    # with open(utility.getCRLPath(), 'rb') as f: crlBytes = f.read()
    # crl = utility.loadCRLFromBytes(crlBytes)
    # if isCertInCRL(pyCryptographyCert, crl):
    #     return False

    # Create and fill a X509Sore with trusted certs
    #this section taken from: http://www.yothenberg.com/validate-x509-certificate-in-python/
    store = OpenSSL.crypto.X509Store()
    with open(utility.getCaCertPath()) as f:
        cacertdata = f.read()
    cacert = utility.loadCert_OpenSSL(OpenSSL.crypto.FILETYPE_PEM, cacertdata)
    store.add_cert(cacert)
    store_ctx = OpenSSL.crypto.X509StoreContext(store, pyopensslCert)
    result = store_ctx.verify_certificate()

    return result is None


def testRevoke():
    '''

    :return:
    '''
    certToRevoke = utility.getPiCertPath()
    reason = extensions.ReasonFlags.unspecified
    req = buildRequestData(commands.REVOKE_REQUEST, (certToRevoke, reason))

    sendUpgrade.checkIP("2")
    sendUpgrade.sendAll(["192.168.1.3"], req, commands.KEY_SERVER_REQUEST, ports.KEY_SERVER_PORT)

def testCertInCRL(myPi):
    with open("certs/revocations/pi.pem", 'rb') as f:
        cert = f.read()
    cert = x509.load_pem_x509_certificate(cert, default_backend())
    b = isCertInCRL(cert, myPi)
    print(b)
