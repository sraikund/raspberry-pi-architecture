#Susan Ni
#6/26/18
#test server with CRL implementation

import cryptography
import utility
import OpenSSL
import socket, ssl

def createServerSideSocket(port, backlog=5):
    context = ssl.create_default_context(purpose=ssl.Purpose.CLIENT_AUTH)
    context.load_cert_chain(certfile=utility.getPiCertPath(), keyfile=utility.getPiKeyPath(), password='password')
    context.verify_flags = ssl.VERIFY_CRL_CHECK_LEAF #also could be VERIFY_CRL_CHECK_CHAIN
    context.load_verify_locations(cafile = utility.getCRLPath())

    sock = socket.socket()
    sock.bind(('', port))
    sock.listen(backlog)

    return sock, context

def createClientSideSocket(server_ip):
    context = ssl.create_default_context(purpose=ssl.Purpose.SERVER_AUTH, cafile=utility.getCaCertPath())
    context.verify_flags = ssl.VERIFY_CRL_CHECK_LEAF #also could be VERIFY_CRL_CHECK_CHAIN; hypothesizing verify_flags tells load_verify_locations to check with CRLs
    context.load_verify_locations(cafile = utility.getCRLPath())

    conn = context.wrap_socket(socket.socket(), server_hostname=server_ip)
    return conn

def acceptSocket(sock, context, func, args):
    assert isinstance(sock, socket.socket)
    assert isinstance(context, ssl.SSLContext)

    while True:
        # wait for an incoming connection, create a new socket, and wrap it
        # using the provided context
        newSocket, fromAddr = sock.accept()
        connStream = context.wrap_socket(newSocket, server_side=True)
        # call the provided function, closing the socket when the function exits
        try:
            func(connStream, *args)
        finally:
            try:
                connStream.shutdown(socket.SHUT_RDWR)
            except OSError as e:
                pass
            connStream.close()


port = 5000

sock, context = createServerSideSocket(port)
print('socket has been created')

def receive(sock):
    print("data received:")
    print(bytes.decode(sock.recv(1024)))

try:
    acceptSocket(sock, context, receive, ())
    print('accepted') #never reaches this because socketUtil.acceptSocket is a while True loop
except Exception as e:
    print(e)